NAME=wt-zsh-config
VERSION=1.3.1

PKG_NAME=$(NAME)
PKG_ARCH=all

SYSCONF=/etc
PREFIX?=/usr/share
DOC_DIR=$(PREFIX)/doc/$(PKG_NAME)

BLD_DIR=$(PKG_NAME)_$(VERSION)_$(PKG_ARCH)
PKG=$(BLD_DIR).deb

PUBKEY_ID=EE8BEF82958D1FF33F3B375EBBBB9FB338982697

pkg: build
	dpkg --build ${BLD_DIR}

build:
	git submodule update --init --recursive
	mkdir -p $(BLD_DIR)/$(DOC_DIR)
	mkdir -p $(BLD_DIR)/etc/zsh
	mkdir -p $(BLD_DIR)/usr/share/zsh/themes
	mkdir -p $(BLD_DIR)/usr/share/zsh/plugins
	cp config/zshrc.new $(BLD_DIR)/etc/zsh
	cp config/zshenv.new $(BLD_DIR)/etc/zsh
	cp -r config/oh-my-zsh $(BLD_DIR)/usr/share
	cp -r config/plugins/* $(BLD_DIR)/usr/share/zsh/plugins
	cp -r config/themes/* $(BLD_DIR)/usr/share/zsh/themes
	cp -r DEBIAN $(BLD_DIR)
	cp LICENSE $(BLD_DIR)/$(DOC_DIR)

sign: pkg
	dpkg-sig -k $(PUBKEY_ID) --sign wiltech $(PKG)
	dpkg-sig -c $(PKG)

clean:
	rm -rf $(PKG) $(BLD_DIR)

all: pkg sign

tag:
	git tag $(VERSION)
	git push --tags

release: pkg sign tag

install:
	apt install ./$(PKG)

uninstall:
	apt remove --purge $(PKG_NAME)

.PHONY: build sign clean test tag release install uninstall all
